package kz.ruslovislo.pojo.table;


import kz.ruslovislo.exceptions.RequestCriteriaError;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.criteria.*;
import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.util.*;

@SuppressWarnings({ "serial", "rawtypes" })
public class Filter implements Specification {

    @Getter
    @Setter
    private Rules rules;

    @Getter
    @Setter
    private Class dto;

//    public Filter(String json) {
////        ObjectMapper mapper = new ObjectMapper();
////        this.conditions = mapper.readValue(json, mapper.getTypeFactory().constructCollectionType(List.class, Condition.class));
//    }

    public Filter(Class dto, Rules conditions) {
        joined.clear();
        this.rules=conditions;
        this.dto=dto;
//        conditions = new ArrayList<>();
    }

//    public void addCondition(Condition condition) {
//        this.conditions.add(condition);
//    }


//    private void joinable(Root root) {
//
//		SingularAttributeImpl attr = (SingularAttributeImpl)root.getModel().getAttribute("communicationNode");
//		attr.isOptional()
//
//
//
//		root.fetch("communicationNode", JoinType.LEFT);
//
//
//    }
//



    private Set<String> joined = new HashSet<>();

    private void joines(Root root, Rules rules) {
        if(rules.getConditions()!=null) {
            for(Condition c:rules.getConditions()) {
                String fn=null;
                if(c.field.indexOf(".")==-1)
                    fn = c.field;
                else
                    fn = c.field.substring(0, c.field.indexOf("."));
                if(joined.contains(fn))
                    continue;


                Field f;
                try {
                    f = root.getJavaType().getDeclaredField(fn);
                    ManyToOne m = f.getDeclaredAnnotation(ManyToOne.class);
                    if(m!=null && m.optional()) {
                        root.fetch(fn, JoinType.LEFT);
                        joined.add(fn);
                    }else{
                        OneToOne m2 = f.getDeclaredAnnotation(OneToOne.class);
                        if(m2!=null){
                            root.fetch(fn, JoinType.LEFT);
                            joined.add(fn);
                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }





//    			SingularAttributeImpl attr = (SingularAttributeImpl)root.getModel().getAttribute(fn);
//    			if(attr.isOptional()) {
//    				root.fetch(fn, JoinType.LEFT);
//    				joined.add(fn);
//    			}
            }
        }
        if(rules.getRules()!=null) {
            for(Rules r:rules.getRules()) {
                joines(root,r);
            }
        }

    }


    @Override
    public Predicate toPredicate(Root root, CriteriaQuery criteriaQuery, CriteriaBuilder criteriaBuilder) {
//    	joines(root,rules);
        try {
            return toPrd(rules, root, criteriaQuery, criteriaBuilder);
        } catch (Exception e) {
            e.printStackTrace();
            throw new RequestCriteriaError("Error building criteria. "+e.getMessage(),e);
        }

    }



    public Predicate toPrd(Rules conditions, Root root, CriteriaQuery criteriaQuery, CriteriaBuilder criteriaBuilder) throws Exception{

        List<Predicate> predicates = new ArrayList<>();

        for(Condition condition:conditions.getConditions()) {
            predicates.add(buildPredicate(condition, root, criteriaQuery, criteriaBuilder));
        }
        for(Rules sub:conditions.getRules()) {
            predicates.add(toPrd(sub, root, criteriaQuery, criteriaBuilder));
        }

        if(predicates.size()==0)
            return null;

        if(conditions.getOperation()==Operation.AND) {

            return predicates.size() > 1
                    ? criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]))
                    : predicates.get(0);
        }else {
            return predicates.size() > 1
                    ? criteriaBuilder.or(predicates.toArray(new Predicate[predicates.size()]))
                    : predicates.get(0);
        }


    }
    /*
      private List<Predicate> buildPredicates(Root root, CriteriaQuery criteriaQuery, CriteriaBuilder criteriaBuilder) {
  //       return conditions.stream().map(this::buildPredicate).collect(toList());
          List<Predicate> predicates = new ArrayList<>();

          for(Condition condition:conditions.getConditions()) {
              predicates.add(buildPredicate(condition, root, criteriaQuery, criteriaBuilder));
          }

          conditions.forEach(condition -> predicates.add(buildPredicate(condition, root, criteriaQuery, criteriaBuilder)));
          return predicates;
      }
  */
    public Predicate buildPredicate(Condition condition, Root root, CriteriaQuery criteriaQuery, CriteriaBuilder criteriaBuilder) throws Exception{
        if(condition.getField()!=null)
            condition.setField(TableDataRequest.getEntityFieldName(dto, condition.field));

        switch (condition.comparison) {
            case eq:
                return buildEqualsPredicateToCriteria(condition, root, criteriaQuery, criteriaBuilder);
            case like:
                return buildLikePredicateToCriteria(condition, root, criteriaQuery, criteriaBuilder);
            case gt:
                return buildGreaterThanPredicateToCriteria(condition, root, criteriaQuery, criteriaBuilder);
            case lt:
                return buildLessThanPredicateToCriteria(condition, root, criteriaQuery, criteriaBuilder);
            case ne:
                return buildNotEqualsPredicateToCriteria(condition, root, criteriaQuery, criteriaBuilder);
            case isnull:
                return buildIsNullPredicateToCriteria(condition, root, criteriaQuery, criteriaBuilder);
            case notnull:
                return buildIsNotNullPredicateToCriteria(condition, root, criteriaQuery, criteriaBuilder);
            case in:
                return buildInPredicateToCriteria(condition, root, criteriaQuery, criteriaBuilder);
            case notin:
                return buildNotInPredicateToCriteria(condition, root, criteriaQuery, criteriaBuilder);
            default:
                return buildEqualsPredicateToCriteria(condition, root, criteriaQuery, criteriaBuilder);
        }
    }


    private Date getDate(Condition condition)throws Exception{
        if(condition.pattern==null || condition.pattern.trim().length()==0)
            throw new Exception("Не указан формат даты");
        SimpleDateFormat sdf =new SimpleDateFormat(condition.pattern);
        Date d = sdf.parse((String)condition.value);
        return d;
    }
    private Calendar getCalendar(Condition condition)throws Exception{
        Calendar cal = Calendar.getInstance();
        cal.setTime(getDate(condition));
        return cal;
    }
    private Object getEnum(Class enumClass, Condition condition)throws Exception{
        return Enum.valueOf(enumClass, (String) condition.getValue());
    }

    //
    private static boolean isOptinal(Root root, String field) throws Exception{
        Field f = null;

        Class cls = root.getJavaType();

        int owr = 0;
        boolean nnd = true;
        while (nnd){
            owr++;
            if(owr>50)
                throw new NoSuchFieldException("__ "+field);
            try {
                f = cls.getDeclaredField(field);
                nnd = false;
            }catch(NoSuchFieldException e) {
                if(cls.getSuperclass()!=null){
                    cls = cls.getSuperclass();
                }else{
                    nnd = false;
                    throw  e;
                }

//                f = root.getJavaType().getSuperclass().getDeclaredField(field);
            }

        }






        ManyToOne m = f.getDeclaredAnnotation(ManyToOne.class);
        if(m!=null && m.optional()) {

            return true;

        }
        return false;
    }

    private static boolean isCollection(Root root, String field) throws Exception{
        Field f = null;
        try {
            f = root.getJavaType().getDeclaredField(field);
        }catch(NoSuchFieldException e) {
            f = root.getJavaType().getSuperclass().getDeclaredField(field);
        }

        OneToMany m = f.getDeclaredAnnotation(OneToMany.class);
        if(m!=null) {

            return true;

        }
        return false;
    }

    private static Class getFieldType(Root root, String field) throws Exception{
        Field f = null;
        try {
            f = root.getJavaType().getDeclaredField(field);
        }catch(NoSuchFieldException e) {
            f = root.getJavaType().getSuperclass().getDeclaredField(field);
        }
        if(f==null)
            return null;
        return f.getType();
    }



    public static Expression getField(Root root, String field)throws Exception {
        if(field.indexOf(".")==-1) {
            if(isOptinal(root, field))
                return root.join(field,JoinType.LEFT);
            else {
                return root.get(field);
            }
        }

        Path p = root;

        String f = field;
        boolean begin = true;
        while(f.indexOf(".")!=-1) {
            String ff = f.substring(0, f.indexOf("."));

            if(begin && isOptinal(root, ff)) {
                p = root.join(ff,JoinType.LEFT);
            }else {
                if(begin && isCollection(root, ff))
                    p = root.join(ff,JoinType.INNER);
                else
                    p = p.get(ff);
            }
            f = f.substring(f.indexOf(".")+1);
            begin = false;
        }


        p = p.get(f);


        return p;

    }

    private Object getConditionValue(Class type, Condition condition)throws Exception{
        if(condition.value == null)return null;
        if(condition.value instanceof String){
            if(((String)condition.value).trim().length()==0)return null;
        }

        try {

            if(type.getName().equals(Long.class.getName()) || type.getName().equals(long.class.getName()))
                return Integer.parseInt((String)(condition.value+""));
            else if(type.getName().equals(Integer.class.getName()) || type.getName().equals(int.class.getName()))
                return Integer.parseInt((String)(condition.value+""));
            else if(type.getName().equals(Double.class.getName()) || type.getName().equals(double.class.getName()))
                return Double.parseDouble((String)(condition.value+""));

            else if(type.getName().equals(boolean.class.getName()) || type.getName().equals(Boolean.class.getName()))
                return Boolean.parseBoolean((String)condition.value);

            else if(type.getName().equals(Date.class.getName())){
                return getDate(condition);
            }
            else if(type.getName().equals(Calendar.class.getName())){
                return getCalendar(condition);
            }
            else if(type.isEnum()){
                return Enum.valueOf((Class<? extends Enum>)Class.forName(type.getName()), (String)condition.value);
            }

            throw new Exception("Not defined type: "+type.getName());

        }catch (Exception e){
            throw new Exception("Error cast value ["+condition.value+"] to type ["+type.getName()+"]",e);
        }
    }

    private Predicate buildEqualsPredicateToCriteria(Condition condition, Root root, CriteriaQuery criteriaQuery, CriteriaBuilder criteriaBuilder) throws Exception{

        Expression fld = getField(root, condition.field);
        if(condition.type==null){
            return criteriaBuilder.equal(fld, getConditionValue(fld.getJavaType(), condition));
        }

        switch(condition.type) {
            case string:
                return criteriaBuilder.equal(criteriaBuilder.upper(fld), ((String)condition.value).toUpperCase());
            case bool:
                return criteriaBuilder.equal(fld, Boolean.parseBoolean((String)condition.value));
            case numeric_integer:
                return criteriaBuilder.equal(fld, ((Integer)condition.value));
            case numeric_double:
                return criteriaBuilder.equal(fld, ((Double)condition.value));
            case numeric_long:
                return criteriaBuilder.equal(fld, ((Integer)condition.value));
            case date:
                return criteriaBuilder.equal(fld, getDate(condition));
            case calendar:
                return criteriaBuilder.equal(fld, getCalendar(condition));
            case enumer:
                return criteriaBuilder.equal(fld, Enum.valueOf((Class<? extends Enum>)Class.forName(condition.enumClass), (String)condition.value));
            default:
                throw new Exception("Not implemented yet");
        }

//        return criteriaBuilder.equal(root.get(condition.field), condition.value);
    }
    private Predicate buildNotEqualsPredicateToCriteria(Condition condition, Root root, CriteriaQuery criteriaQuery, CriteriaBuilder criteriaBuilder) throws Exception{

        Expression fld = getField(root, condition.field);
        if(condition.type==null){
            return criteriaBuilder.notEqual(fld, getConditionValue(fld.getJavaType(), condition));
        }

        switch(condition.type) {
            case string:
                return criteriaBuilder.notEqual(criteriaBuilder.upper(getField(root, condition.field)), ((String)condition.value).toUpperCase());
            case numeric_integer:
                return criteriaBuilder.notEqual((getField(root, condition.field)), ((Integer)condition.value));
            case numeric_double:
                return criteriaBuilder.notEqual((getField(root, condition.field)), ((Double)condition.value));
            case numeric_long:
                return criteriaBuilder.notEqual((getField(root, condition.field)), ((Integer)condition.value));
            case date:
                return criteriaBuilder.notEqual((getField(root, condition.field)), getDate(condition)  );
            case calendar:
                return criteriaBuilder.notEqual((getField(root, condition.field)), getCalendar(condition)  );
            case enumer:
                return criteriaBuilder.notEqual((getField(root, condition.field)), Enum.valueOf((Class<? extends Enum>)Class.forName(condition.enumClass),(String)condition.value));
            default:
                throw new Exception("Not implemented yet");
        }

//        return criteriaBuilder.equal(root.get(condition.field), condition.value);
    }
    private Predicate buildLikePredicateToCriteria(Condition condition, Root root, CriteriaQuery criteriaQuery, CriteriaBuilder criteriaBuilder) throws Exception{
        Expression fld = getField(root, condition.field);
        if(!fld.getJavaType().getName().equals(String.class.getName()))
            throw new Exception("LIKE comparison for field '"+condition.field+"' is not applicable. This field of type "+fld.getJavaType().getName());
        return criteriaBuilder.like(criteriaBuilder.upper(fld), ((String)condition.value).toUpperCase());
        //return criteriaBuilder.equal(root.get(condition.field), condition.value);
    }

    private Predicate buildIsNullPredicateToCriteria(Condition condition, Root root, CriteriaQuery criteriaQuery, CriteriaBuilder criteriaBuilder) throws Exception{
        return criteriaBuilder.isNull(getField(root, condition.field));
        //return criteriaBuilder.equal(root.get(condition.field), condition.value);
    }

    private Predicate buildIsNotNullPredicateToCriteria(Condition condition, Root root, CriteriaQuery criteriaQuery, CriteriaBuilder criteriaBuilder) throws Exception{
        return criteriaBuilder.isNotNull(getField(root, condition.field));
        //return criteriaBuilder.equal(root.get(condition.field), condition.value);
    }
    private Predicate buildGreaterThanPredicateToCriteria(Condition condition, Root root, CriteriaQuery criteriaQuery, CriteriaBuilder criteriaBuilder) throws Exception{
        switch(condition.type) {
            case numeric_integer:
                return criteriaBuilder.gt(getField(root, condition.field), (Integer)condition.value);
            case numeric_double:
                return criteriaBuilder.gt(getField(root, condition.field), (Double)condition.value);
            case numeric_long:
                return criteriaBuilder.gt(getField(root, condition.field), (Integer)condition.value);
            case date:
                return criteriaBuilder.greaterThan(getField(root, condition.field).as(Date.class), (Date) getDate(condition));
//	    		return criteriaBuilder.gt(getField(root, condition.field),(Date) getDate(condition));// criteriaBuilder.greaterThan(getField(root, condition.field), getDate(condition));
            case calendar:
                return criteriaBuilder.greaterThan((getField(root, condition.field)), getCalendar(condition)  );
            default:
                throw new Exception("Not implemented yet");
        }
        //return criteriaBuilder.equal(root.get(condition.field), condition.value);
    }

    private Predicate buildLessThanPredicateToCriteria(Condition condition, Root root, CriteriaQuery criteriaQuery, CriteriaBuilder criteriaBuilder) throws Exception{
        switch(condition.type) {
            case numeric_integer:
                return criteriaBuilder.lt(getField(root, condition.field), (Integer)condition.value);
            case numeric_double:
                return criteriaBuilder.lt(getField(root, condition.field), (Double)condition.value);
            case numeric_long:
                return criteriaBuilder.lt(getField(root, condition.field), (Integer)condition.value);
            case date:
                return criteriaBuilder.lessThan(getField(root, condition.field), getDate(condition));
            case calendar:
                return criteriaBuilder.lessThan((getField(root, condition.field)), getCalendar(condition)  );

            default:
                throw new Exception("Not implemented yet");
        }
    }

    private Predicate buildInPredicateToCriteria(Condition condition, Root root, CriteriaQuery criteriaQuery, CriteriaBuilder criteriaBuilder) throws Exception{
        switch(condition.type) {
            case numeric_long:
                return criteriaBuilder.in(getField(root, condition.field)).value((Integer)condition.value);
            default:
                throw new Exception("Not implemented yet");
        }
    }

    private Predicate buildNotInPredicateToCriteria(Condition condition, Root root, CriteriaQuery criteriaQuery, CriteriaBuilder criteriaBuilder) throws Exception{
        switch(condition.type) {
            case numeric_long:
                return criteriaBuilder.not(criteriaBuilder.in(getField(root, condition.field)).value((Integer)condition.value));
            default:
                throw new Exception("Not implemented yet");
        }
    }


    public static void main(String[] args) {



      /*  String s = "aa.ss.dd";

        try {
            Class cc = StrataStateEnum.class;
            Object o = Enum.valueOf(cc, "CONNECTED");
            System.out.println(((StrataStateEnum)o).getDescription());


            Field f =OtTransferCounter.class.getSuperclass().getDeclaredField("createdDate");
            System.out.println(f.getType().getName());

        } catch (NoSuchFieldException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (SecurityException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        System.out.println(s.indexOf("."));
        System.out.println(s.substring(0,s.indexOf(".")));
        System.out.println(s.substring(s.indexOf(".")+1));*/
    }
}
