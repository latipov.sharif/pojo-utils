package kz.ruslovislo.pojo.table;

public enum Type {
    numeric_integer,
    numeric_double,
    numeric_long,
    string,
    enumer,
    date /* 'yyyy-MM-dd HH:mm:ss'  OR 'yyyy-MM-dd'  */,
    calendar /* 'yyyy-MM-dd HH:mm:ss'  OR 'yyyy-MM-dd'  */,
    bool,
    uuid,
    list,
    raw
}
