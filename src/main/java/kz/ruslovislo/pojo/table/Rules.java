package kz.ruslovislo.pojo.table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

/**
 * The JSON object that defines filtering conditions.
 *
 * @example
 * {
 *     "operation":"AND",
 *     "conditions":[
 *          {...},
 *          {...}
 *     ]
 * }
 *
 * @see Condition
 *
 * @name Rules
 */
@SuppressWarnings("serial")
public class Rules implements Serializable{

    /**
     * Comparison operation to compare the rule conditions. The value can take from options [AND, OR].
     */
    @Getter
    @Setter
    private Operation operation = Operation.AND;
    /**
     * Rule conditions.
     */
    @Getter
    @Setter
    private List<Condition> conditions = new LinkedList<>();

    @Getter
    @Setter
    private List<Rules> rules = new LinkedList<>();

    @JsonIgnore
    public int getRulesHash(){

        String s = ""+operation.name();
        if(conditions!=null && conditions.size()>0){
            for(Condition condition:conditions){
                s=s+condition.getField()+condition.getPattern()+(condition.getComparison()==null?"":condition.getComparison().name())+(condition.getType()==null?"":condition.getType().name())+condition.getValue();
            }
        }
        if(rules!=null && rules.size()>0){
            for(Rules rule:rules){
                s=s+rule.getRulesHash();
            }
        }
        return s.hashCode();
    }

}
