package kz.ruslovislo.pojo.table;

public enum Comparison {
    // equal
    eq,
    // greaterThan
    gt,
    // lowerThan
    lt,
    // not equal
    ne,
    // is null
    isnull,

    // is not null
    notnull,

    in,
    notin,
    like
}
