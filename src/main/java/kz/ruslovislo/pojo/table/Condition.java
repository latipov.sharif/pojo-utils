package kz.ruslovislo.pojo.table;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * The object that define single field filtering condition.
 * @example
 */
@SuppressWarnings("serial")
public class Condition implements Serializable{

    /**
     * The field type.
     */
    @Getter
    @Setter
    public Type type;
    @Getter
    @Setter
    public Comparison comparison;
    @Getter
    @Setter
    public Object value;
    @Getter
    @Setter
    public String field;

    @Getter
    @Setter
    public String pattern;

    @Getter
    @Setter
    public String enumClass;

    public Condition() {
    }

    public Condition(Type type, Comparison comparison, Object value, String field) {
        this.type = type;
        this.comparison = comparison;
        this.value = value;
        this.field = field;
    }

    public Condition(Type type, Comparison comparison, Object value, String field, String pattern) {
        this.type = type;
        this.comparison = comparison;
        this.value = value;
        this.field = field;
        this.pattern = pattern;
    }


    public Condition(Type type, Comparison comparison, Object value, String field, String pattern, String enumClass) {
        this.type = type;
        this.comparison = comparison;
        this.value = value;
        this.field = field;
        this.pattern=pattern;
        this.enumClass = enumClass;
    }
    public static class Builder {
        private Type type;
        private Comparison comparison;
        private Object value;
        private String field;
        private String pattern;

        public Builder setType(Type type) {
            this.type = type;
            return this;
        }

        public Builder setComparison(Comparison comparison) {
            this.comparison = comparison;
            return this;
        }

        public Builder setValue(Object value) {
            this.value = value;
            return this;
        }

        public Builder setField(String field) {
            this.field = field;
            return this;
        }

        public void setPattern(String pattern) {
            this.pattern = pattern;
        }

        public Condition build() {
            return new Condition(type, comparison, value, field, pattern);
        }
    }



}
