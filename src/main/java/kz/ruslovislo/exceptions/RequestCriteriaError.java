package kz.ruslovislo.exceptions;

public class RequestCriteriaError extends RuntimeException{

    public RequestCriteriaError() {
    }

    public RequestCriteriaError(String message) {
        super(message);
    }

    public RequestCriteriaError(String message, Throwable cause) {
        super(message, cause);
    }

    public RequestCriteriaError(Throwable cause) {
        super(cause);
    }

    public RequestCriteriaError(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
