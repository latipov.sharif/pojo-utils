package kz.ruslovislo;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author Ruslan Temirbulatov on 12/17/19
 * @project pojo-utils
 */
public class Test {

    private String pubCodeMaskCity = "AL";
    private String pubCodeMaskDate = "YYMM";
    private String pubCodeMaskNumbers = "xxx";
    private String pubCodeMaskFormat = "{date}{city}{numbers}";

    public String generateAlarmPublicCode(String lastPublicCode) throws Exception {



        int i1 = pubCodeMaskFormat.indexOf("{city}");
        int l1 = pubCodeMaskCity.length();
        int x1 = 0;

        int i2 = pubCodeMaskFormat.indexOf("{date}");
        int l2 = pubCodeMaskDate.length();
        int x2 = 0;

        int i3 = pubCodeMaskFormat.indexOf("{numbers}");
        int l3 = pubCodeMaskNumbers.length();
        int x3 = 0;

        if(i1==0){
            if(i2<i3){
                x2 = l1;
                x3 = x2 + l2;
            }else{
                x3=l1;
                x2 = x3+l3;
            }
        }else if(i2==0){
            if(i1<i3){
                x1 = l2;
                x3 = x1+l1;
            }else{
                x3 = l2;
                x1 = x3+l3;
            }
        }else{
            if(i1<i2){
                x1 = l3;
                x2 = x1+l1;
            }else{
                x2 = l3;
                x1 = x2 + l2;
            }
        }

        String city = lastPublicCode.substring(x1,x1+l1);
        String date = lastPublicCode.substring(x2,x2+l2);
        String dateCurr = new SimpleDateFormat(pubCodeMaskDate).format(new Date());
        String numbers = lastPublicCode.substring(x3,x3+l3);

        Integer inumber = Integer.parseInt(numbers)+1;
        if(!(pubCodeMaskCity.equals(city) && dateCurr.equals(date))){
            inumber = 1;
        }

        numbers = ""+inumber;
        while(numbers.length()<l3)
            numbers="0"+numbers;

        System.out.println("city "+city);
        System.out.println("date "+date);
        System.out.println("numbers "+numbers);

        return pubCodeMaskFormat.replaceAll("\\{city}",pubCodeMaskCity).replaceAll("\\{date}",dateCurr).replaceAll("\\{numbers}",numbers);
    }

    public static void main(String[] args) throws Exception {
//        new Test().generateAlarmPublicCode("AL1901002");
//        System.out.println(new Test().generateAlarmPublicCode("1912AL012"));


//        System.out.println(new SimpleDateFormat("yyyy-MM-dd").parse("2020-04-15").getTime());


        List<String> list = new ArrayList<>();
        list.add("sss");

        list.forEach( x -> {
            System.out.println(x);
        });

        list.forEach(System.out::println);

    }
}
