package kz.ruslovislo.jpa;

public interface CustomRepository<T> {

    void refresh(T t);
}
