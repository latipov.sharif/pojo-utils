package kz.ruslovislo.jpa;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

public class CustomRepositoryImpl<T> implements CustomRepository<T>{

    @PersistenceContext
    EntityManager entityManager;

    @Override

    public void refresh(T t) {
        entityManager.refresh(t);
    }
}
