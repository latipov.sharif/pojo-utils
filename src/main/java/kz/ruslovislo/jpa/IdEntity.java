package kz.ruslovislo.jpa;

/**
 * @author Ruslan Temirbulatov on 10/17/19
 * @project pojo-utils
 */

import java.io.Serializable;
import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.stereotype.Indexed;

/**
 * The basic object of all custom jpa entities.
 * @author Ruslan Temirbulatov
 * @xxx bla bla bla
 *
 */
@SuppressWarnings("serial")
@MappedSuperclass
public class IdEntity implements Serializable{

    @Getter
    @Setter
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name="id")
    private Long id;

/*
    @Id
//    @javax.persistence.Id
//    @Indexed(name="id", type="string")
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(columnDefinition = "CHAR(36)")
    @Getter
    @Setter
    private String id;

*/


    @Getter
    @Setter
    @Column(updatable=false)
    @Temporal(TemporalType.TIMESTAMP)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", locale = "en_US")
    private Calendar createdDate;

    @Getter
    @Setter
    @Temporal(TemporalType.TIMESTAMP)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", locale = "en_US")
    private Calendar modificatedDate;



    @PreUpdate
    protected void preUpdate() {
        modificatedDate = Calendar.getInstance();
    }


    @PrePersist
    protected void prePersist() {
        createdDate = Calendar.getInstance();
        modificatedDate = Calendar.getInstance();
    }
}
