package kz.ruslovislo.jpa.empty;

/**
 * @author Ruslan Temirbulatov on 10/17/19
 * @project pojo-utils
 */
import kz.ruslovislo.jpa.uuid.UuidEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface EmptyAbstractRepository<T extends EmptyEntity, ID> extends JpaRepository<T, ID>, JpaSpecificationExecutor<T> {
}

