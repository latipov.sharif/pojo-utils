package kz.ruslovislo.jpa.empty;

import javax.persistence.MappedSuperclass;

/**
 * @author Ruslan Temirbulatov on 12/15/19
 * @project pojo-utils
 */
@MappedSuperclass
public class EmptyEntity {
}
