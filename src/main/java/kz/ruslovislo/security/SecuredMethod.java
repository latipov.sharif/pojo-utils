package kz.ruslovislo.security;

import lombok.Data;

@Data
public class SecuredMethod {

    private String method;
    private String description;
}
