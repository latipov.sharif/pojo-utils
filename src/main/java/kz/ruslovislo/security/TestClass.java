package kz.ruslovislo.security;

import javax.annotation.security.RolesAllowed;

public class TestClass {

    @RolesAllowed({"AA"})
    @MethodAllowedDescription("aaaaaa")
    public void aa(){}

    @RolesAllowed({"BB"})
    @MethodAllowedDescription("bbbbbb")
    public void bb(){}

    @RolesAllowed({"CC"})
    public void cc(){}


    public void dd(){}

}
