package kz.ruslovislo.security;

import javax.annotation.security.RolesAllowed;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;
import java.net.URL;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.LinkedList;
import java.util.List;

public class SecurityUtils {

    public static List<SecuredMethod> scanPackage(String packageName)throws Exception{
        List<SecuredMethod> list = new LinkedList<>();
        Class[] clss = getClasses(packageName);
        if(clss!=null)
            for(Class cls:clss)
                list.addAll(scanClass(cls));

        return list;
    }

    public static List<SecuredMethod> scanClass(Class cls)throws Exception{
        List<SecuredMethod> list = new LinkedList<>();
        Method[] mts = cls.getDeclaredMethods();
        if(mts!=null){
            for(Method mt:mts){
                RolesAllowed rolesAllowed = mt.getDeclaredAnnotation(RolesAllowed.class);
                if(rolesAllowed==null)
                    continue;
                MethodAllowedDescription description = mt.getDeclaredAnnotation(MethodAllowedDescription.class);

                SecuredMethod securedMethod = new SecuredMethod();

                if(rolesAllowed.value().length>1)
                    throw new Exception("The method "+cls.getName()+"."+mt.getName()+" has annotation @RolesAllowed with more than one values");
                securedMethod.setMethod(rolesAllowed.value()[0]);
                if(description!=null)
                    securedMethod.setDescription(description.value());
                else
                    securedMethod.setDescription(securedMethod.getMethod());
                list.add(securedMethod);

            }
        }

        return list;
    }


    private static Class[] getClasses(String packageName)
            throws ClassNotFoundException, IOException {
        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        assert classLoader != null;
        String path = packageName.replace('.', '/');
        Enumeration<URL> resources = classLoader.getResources(path);
        List<File> dirs = new ArrayList<>();
        while (resources.hasMoreElements()) {
            URL resource = resources.nextElement();
            dirs.add(new File(resource.getFile()));
        }
        ArrayList<Class> classes = new ArrayList<Class>();
        for (File directory : dirs) {
            classes.addAll(findClasses(directory, packageName));
        }
        return classes.toArray(new Class[classes.size()]);
    }

    private static List<Class> findClasses(File directory, String packageName) throws ClassNotFoundException {
        List<Class> classes = new ArrayList<Class>();
        if (!directory.exists()) {
            return classes;
        }
        File[] files = directory.listFiles();
        for (File file : files) {
            if (file.isDirectory()) {
                assert !file.getName().contains(".");
                classes.addAll(findClasses(file, packageName + "." + file.getName()));
            } else if (file.getName().endsWith(".class")) {
                classes.add(Class.forName(packageName + '.' + file.getName().substring(0, file.getName().length() - 6)));
            }
        }
        return classes;
    }

    public static void main(String[] args) throws Exception {
        List<SecuredMethod> xx = scanClass(TestClass.class);
        for(SecuredMethod x:xx){
            System.out.println(x.getMethod()+"  "+x.getDescription());
        }
        System.out.println("-----------");
        xx = scanPackage("kz.ruslovislo.security");
        for(SecuredMethod x:xx){
            System.out.println(x.getMethod()+"  "+x.getDescription());
        }
    }
}
