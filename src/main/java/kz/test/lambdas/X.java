package kz.test.lambdas;

public class X {

    private ActionListener listener;

    public void addListener(ActionListener listener){
        this.listener=listener;
    }

    public void run(String s){
        listener.actionPerformed(s);
    }


}
