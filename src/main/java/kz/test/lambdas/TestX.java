package kz.test.lambdas;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.IntSummaryStatistics;
import java.util.LinkedList;
import java.util.List;
import java.util.function.BinaryOperator;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class TestX {

    private static Logger log = LoggerFactory.getLogger("aa");

    private static void debug(Supplier<String> message){
        if(log.isDebugEnabled())
            log.debug(message.get());
    }

    public static void main(String[] args) {
        X x = new X();



       /* x.addListener(new ActionListener() {
            @Override
            public void actionPerformed(String event) {
                System.out.println("--- "+event);
            }



        });
*/


       String name = "ruslan";

       ActionListener actionListener = xx -> System.out.println("8888888888  "+xx+" "+name);

       x.addListener(actionListener);

       // x.addListener((event) -> {System.out.println("===== "+event);});





        x.run("fffffff");

        Predicate<Integer> atLeast5 = d -> d > 5;
        System.out.println(atLeast5.test(4));

        BinaryOperator<Long> add = (a,b) -> {return a+b;};

        System.out.println(add.apply(1L,2L));

        List<Integer> list = new LinkedList<>();
        list.add(1);
        list.add(2);
        list.add(3);
        list.add(4);
        list.add(5);
        list.add(6);


        System.out.println("count "+list.stream().filter(item -> item> 4).count());


        List<String> collected = Stream.of("a", "b", "hello")
                .map(string -> string.toUpperCase())
                .collect(Collectors.toList());
        System.out.println(collected);

        debug(()->"skjfhkdshfsjkdfsd");

        IntSummaryStatistics intSummaryStatistics = list.stream().mapToInt(p -> p.intValue()).summaryStatistics();
        System.out.println("max "+intSummaryStatistics.getMax());
        System.out.println("avg "+intSummaryStatistics.getAverage());


    }
}
