package kz.test.lambdas;

@FunctionalInterface
public interface ActionListener {

    public void actionPerformed(String event);


}
